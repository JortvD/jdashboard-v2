var cerus = require("cerus")();

cerus.use(require("../index")({
	name: "martema",
	title: "martema - Dashboard"
}));
cerus.use(require("cerus-database")());

var db = cerus.database();
db.driver("mongo");
//db.connect("localhost", );
db = db.database("oma_test");

var dashboard = cerus.dashboard();

dashboard.user("Marije", "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918", {
	"role": "Eigenaar"
});

var sieraden = dashboard.section("Sieraden", "sieraden");
sieraden.event()
.on("load", function(section) {
	var table = db.table("sieraden");
	table.columns().add("_id");
	table.columns().add("naam");
	table.columns().add("beschrijving");
	table.columns().add("materialen");
	table.columns().add("afbeelding");
	table.columns().add("prijs");
	table.columns().add("type");

	table.find({})
	.then(function(items) {
		for(var i = 0; i < items.length; i++) {
			var item = items[i];
			var page = section.page(item.naam, item._id);
			page.items().set("naam", item.naam);
			page.items().set("beschrijving", item.beschrijving);
			page.items().set("materialen", item.materialen);
			page.items().set("afbeelding", item.afbeelding);
			page.items().set("prijs", item.prijs);
			page.items().set("type", item.type);
		}
	});
})
.on("save", function(page) {
	var table = db.table("sieraden");
	var items = page.items();
	var naam = items.get("naam").value;

	if(naam !== null) {
		page.name(naam);
	}
	
	table.modify({_id: page.id()}, {
		naam: items.get("naam").value || "null",
		beschrijving: items.get("beschrijving").value || "null",
		materialen: items.get("materialen").value || "null",
		afbeelding: items.get("afbeelding").value || "null",
		prijs: items.get("prijs").value || "null",
		type: items.get("type").value || "null"
	});
})
.on("create", function(page) {
	var table = db.table("sieraden");
	var items = page.items();

	table.insert({
		_id: page.id(),
		naam: items.get("naam").value,
		beschrijving: items.get("beschrijving").value,
		materialen: items.get("materialen").value,
		afbeelding: items.get("afbeelding").value,
		prijs: items.get("prijs").value,
		type: items.get("type").value
	});
})
.on("delete", function(page) {
	var table = db.table("sieraden");
	table.delete({_id: page.id()})
});
sieraden.structure().add("Naam", "naam", sieraden.structure().types().text());
sieraden.structure().add("Beschrijving", "beschrijving", sieraden.structure().types().textarea());
sieraden.structure().add("Materialen", "materialen", sieraden.structure().types().textarea());
sieraden.structure().add("Afbeelding", "afbeelding", sieraden.structure().types().image());
sieraden.structure().add("Prijs", "prijs", sieraden.structure().types().number(), {
	step: "0.01",
	min: "0",
	max: "500"
});
sieraden.structure().add("Type", "type", sieraden.structure().types().dropdown(), {
	options: [
		{name: "Ketting", slug: "ketting"},
		{name: "Armband", slug: "armband"}
	]
});

var berichten = dashboard.section("Berichten", "berichten", {
	creatable: false,
	public: true,
	saveable: false
});
berichten.event()
.on("load", function(section) {
	var table = db.table("berichten");
	table.columns().add("_id");
	table.columns().add("naam");
	table.columns().add("email");
	table.columns().add("bericht");

	table.find({})
	.then(function(items) {
		for(var i = 0; i < items.length; i++) {
			var item = items[i];
			var page = section.page(item.naam, item._id);
			page.items().set("naam", item.naam);
			page.items().set("email", item.beschrijving);
			page.items().set("bericht", item.materialen);
		}
	});
})	
.on("delete", function(page) {
	var table = db.table("berichten");
	table.delete({_id: page.id()})
});
berichten.structure().add("Naam", "naam", berichten.structure().types().noinput());
berichten.structure().add("E-Mail", "email", berichten.structure().types().noinput());
berichten.structure().add("Bericht", "bericht", berichten.structure().types().noinput());
cerus.api().add("bericht")
.then(function(req, res) {
	var naam = req.get("naam");
	var email = req.get("email");
	var bericht = req.get("bericht");

	if(naam !== undefined) {
		res.errors().badrequest();
		return;
	}

	var page = berichten.page();
	var items = page.items();
	items.set("naam", naam);
	items.set("email", naam);
	items.set("bericht", naam);

	table.insert({
		_id: page.id(),
		naam: items.get("naam").value,
		email: items.get("email").value,
		bericht: items.get("bericht").value
	});

	res.emit();
});

cerus.server().start().then(function() {
	console.log("Started listening on port " + cerus.server().port());
});