module.exports = function(options) {
	var self = {};

	var cerus = null;
	var path = __dirname + "/";
	var dashboard;

	self.name = "JDashboard";
	self.version = "v0.0.1";

	self.init_ = function(cerus_) {
		cerus = cerus_;

		cerus.use(require("cerus-uuid")());
		cerus.use(require("cerus-sessions")());
		cerus.use(require("cerus-hash")());

		dashboard = require("./lib/dashboard")(cerus, options, path);
	}

	self.dashboard = function() {
		return dashboard;
	}

	return self;
}