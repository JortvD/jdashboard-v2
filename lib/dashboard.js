module.exports = function(cerus, options, path) {
	var self = {};
	
	options = options || {};
	var name = options["name"] || "JDashboard";
	var title = options["title"] || "JDashboard - Dashboard";

	var users = {};
	var sections = {};

	cerus.view("/dashboard", path + "views/dashboard")
	.on("file", function(file) {
		var data = file.data().toString();
		data = data.replace("{TITLE}", title);
		data = data.replace("{NAME}", name);
		file._data.data = data;
	})
	.on("request", function(req, res) {
		if(res.sessions()["username"] === undefined) {
			res.redirect("/login");
		}
	});

	cerus.view("/login", path + "views/login")
	.on("file", function(file) {
		var data = file.data().toString();
		data = data.replace("{TITLE}", title);
		data = data.replace("{NAME}", name);
		file._data.data = data;
	})
	.on("request", function(req, res) {
		if(res.sessions()["username"] !== undefined) {
			res.redirect("/dashboard");
		}
	});

	cerus.assets(path + "assets/js", "js");
	cerus.assets(path + "assets/css", "css");

	cerus.api().add("login")
	.then(function(req, res) {
		req.event()
		.on("end", function() {
			var user;

			if((user = users[req.get("username")]) === undefined) {
				res.errors().badrequest();
				return;
			}

			if(user.password() !== cerus.hash().hash(req.get("password"), "sha256")) {
				res.errors().badrequest();
				return;
			}

			res.json()["code"] = 200;
			res.response().sessions()["username"] = user.username();
			res.emit();
		});
	});

	cerus.api().policy("loggedin")
	.then(function(req, res) {
		if(res.response().sessions()["username"] === undefined) {
			res.errors().unauthorized("You are not logged in!");
			return;
		}
	});

	cerus.api().add("info", "loggedin")
	.then(function(req, res) {
		var user = res.response().sessions()["username"];

		if(user === undefined) {
			res.errors().badrequest();
			return;
		}

		user = users[user];

		res.json()["user"] = {};
		res.json()["user"]["username"] = user.username();
		res.json()["user"]["role"] = user.role();

		var sections_ = {};

		for(var key in sections) {
			var section = sections[key];

			sections_[section.slug()] = {
				"name": section.name(), 
				"slug": section.slug()
			}
		}

		res.json()["sections"] = sections_;
		res.emit();
	});

	self.user = function(username, password, options) {
		var user = require("./user")(cerus, username, password, options);
		users[user.username()] = user;
		return user;
	}

	self.section = function(name, slug, options) {
		var section = require("./section")(cerus, self, name, slug, options);
		sections[section.slug()] = section;
		return section;
	}

	var images = {};
	var images_folder = cerus.folder(cerus.root() + "images/");
	var images_uuids = cerus.uuid();
	var fs = require("fs");

	cerus.router().get("/images/*")
	.then(function(req, res) {
		var uuid = req.url();
		uuid = uuid.replace("/images/", "");
		uuid = uuid.substring(0, uuid.indexOf("."));

		for(var key in images) {
			if(key === uuid) {
				res.type(images[key].type);
				res.send(images[key].data, "binary");
				return;
			}
		}
	});

	images_folder.files()
	.on("file", function(file) {
		file.read()
		.then(function() {
			var uuid = file.name();

			images[uuid] = {
				"name": file.base(),
				"type": file.type(),
				"data": file.data()
			}

			images_uuids.add(uuid);
		});
	});

	self.images = function() {
		var self_ = {};

		self_.add = function(data) {
			var type;

			if(data.substring(0, 30).includes("jpeg")) {
				type = "jpg";
			}
			else if(data.substring(0, 30).includes("png")) {
				type = "png";
			}
			else if(data.substring(0, 30).includes("bmp")) {
				type = "bmp";
			}
			else if(data.substring(0, 30).includes("gif")) {
				type = "gif";
			}
			else {
				type = "jpg";
			}

			var uuid = images_uuids.generate()
			var path = cerus.root() + "images/" + uuid + "." + type;
			var url = "/images/" + uuid + "." + type;
			data = data.replace(/^data:image\/\w+;base64,/, '');

			fs.writeFile(path, data, {encoding: 'base64'}, function (error) {
 				if(error) {
 					throw error;
 				}

 				var file = cerus.file(path);
 				file.read()
 				.then(function() {
 					images[uuid] = {
	 					"name": path,
	 					"type": type,
	 					"data": file.data()
	 				}
 				});
			});

			return url;
		}

		self_.remove = function(url) {
			if(url === null) {
				return;
			}

			var name = url.replace("/images/", "");
			var uuid = name.substring(0, name.indexOf("."));
			var path = cerus.settings().root() + "images/" + name;

			delete images[uuid];

			fs.unlink(path, function(error) {
				if(error) {}
			});
		}

		return self_;
	}

	return self;
}