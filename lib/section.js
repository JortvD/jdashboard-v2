module.exports = function(cerus, dashboard, name, slug, options) {
	var self = {};

	
	options = options || {};
	var creatable = options["creatable"];
	var unique = options["unique"];
	var public = options["public"];
	var saveable = options["saveable"];
	var deleteable = options["deleteable"];

	if(creatable === undefined) {
		creatable = true;
	}

	if(unique === undefined) {
		unique = false;
	}

	if(public === undefined) {
		public = false;
	}

	if(saveable === undefined) {
		saveable = true;
	}

	if(deleteable === undefined) {
		deleteable = true;
	}

	var uuids = cerus.uuid();
	var event;
	var pages = {};
	var structure = require("./structure")(cerus);

	var promise = cerus.promise(function(event_) {
		event = event_;

		event("load", self);
	});

	if(typeof name !== "string") {
		throw new TypeError("argument name must be a string");
	}

	if(typeof slug !== "string") {
		throw new TypeError("argument slug must be a string");
	}

	cerus.api().add(slug + "/get", "loggedin")
	.then(function(req, res) {
		req.event()
		.on("end", function() {
			var page = req.get("page");

			if(page === undefined) {
				res.json()["section"] = {
					"name": name,
					"slug": slug,
					"creatable": creatable,
					"saveable": saveable,
					"deleteable": deleteable,
					"public": public,
					"unique": unique
				}

				var pages_ = {};

				for(var key in pages) {
					var page = pages[key];

					pages_[key] = {
						"name": page.name(),
						"id": page.id()
					}
				}

				res.json()["pages"] = pages_;
				res.emit();
			}
			else {
				page = pages[page];

				if(page === undefined) {
					res.errors().badrequest();
					return;
				}

				res.json()["page"] = {
					"name": page.name(),
					"id": page.id()
				}

				var items = page.items().list();
				var items_ = {};

				for(var key in items) {
					var item = items[key];

					items_[key] = {
						"slug": item.slug,
						"name": item.name,
						"type": item.type,
						"value": item.value,
						"settings": item.settings
					}
				}

				res.json()["page"]["items"] = items_;

				res.emit();
			}
		});
	});

	cerus.api().add(slug + "/save", "loggedin")
	.then(function(req, res) {
		req.event()
		.on("end", function() {
			var page = req.get("page");
			var items = req.get("items");

			if(items === undefined || items === undefined) {
				res.errors().badrequest();
				return;
			}

			page = pages[page];

			if(page === undefined) {
				res.errors().badrequest();
				return;
			}

			for(var key in items) {
				var item = items[key];

				if(page.items().get(key) === undefined) {
					continue;
				}

				if(page.items().get(key).type === structure.types().noinput()) {
					continue;
				}

				if(page.items().get(key).type === structure.types().image() && 
					item.value !== null && item.value.startsWith("data")) {
					dashboard.images().remove(item.original);
					item.value = dashboard.images().add(item.value);
				}

				page.items().set(key, item.value);
			}

			event("save", page);
			res.emit();
		});
	});

	cerus.api().add(slug + "/delete", "loggedin")
	.then(function(req, res) {
		req.event()
		.on("end", function() {
			var page = req.get("page");

			if(page === undefined) {
				res.errors().badrequest();
				return;
			}

			page = pages[page];

			if(page === undefined) {
				res.errors().badrequest();
				return;
			}

			event("delete", page);

			delete pages[page.id()];

			res.emit();
		});
	});

	cerus.api().add(slug + "/create", "loggedin")
	.then(function(req, res) {
		if(!creatable) {
			res.errors().badrequest();
			return;
		}

		var page = self.page("Onbekend");

		event("create", page);

		res.json()["id"] = page.id();
		res.json()["name"] = page.name();
		res.emit();
	});

	self.name = function() {
		return name;
	}

	self.slug = function() {
		return slug;
	}

	self.creatable = function() {
		return creatable;
	}

	self.unique = function() {
		return unique;
	}

	self.public = function() {
		return public;
	}

	self.saveable = function() {
		return saveable;
	}

	self.deleteable = function() {
		return deleteable;
	}

	self.event = function() {
		return promise;
	}

	self.get = function(page) {
		if(page === undefined) {
			return pages;
		}
		else {
			return pages[page];
		}
	}

	self.page = function(name, id) {
		var page = require("./page")(cerus, self, uuids, name, id);
		pages[page.id()] = page;
		return page;
	}

	self.structure = function() {
		return structure;
	}

	return self;
}