module.exports = function(cerus, section, uuids, name, id) {
	var self = {};
	var id = id || uuids.generate();
	var items = {};
	var structure = section.structure();
	var key;

	self.id = function() {
		return id;
	}

	self.name = function(name_) {
		if(name_ !== undefined) {
			name = name_;
		}

		return name;
	}

	self.structure = function() {
		return structure;
	}

	self.items = function() {
		var self_ = {};

		self_.set = function(slug, value) {
			if(value === "null") {
				value = null;
			}

			items[slug].value = value;
		}

		self_.get = function(slug) {
			return items[slug];
		}

		self_.list = function() {
			return items;
		}

		return self_;
	}

	if(section.unique()) {
		structure = require("./structure")(cerus);

		stucture.event()
		.on("add", function(item) {
			items[item.slug] = {
				"slug": item.slug,
				"name": item.name,
				"type": item.type,
				"settings": item.settings,
				"value": item.settings.default || null
			}
		})
		.on("remove", function(item) {
			delete items[item.slug];
		});
	}
	else {
		var items_ = structure.list();
		for(var key in items_) {
			var item = items_[key];

			items[key] = {
				"slug": key,
				"name": item.name,
				"type": item.type,
				"settings": item.settings,
				"value": item.settings.default || null
			}
		}
	}
	
	return self;
}