module.exports = function(cerus, username, password, options) {
	var self = {};
	options = options || {};
	var role = options["role"] || "Eigenaar";
	var avatar = options["avatar"];

	self.username = function() {
		return username;
	}

	self.role = function() {
		return role;
	}

	self.avatar = function() {
		return avatar;
	}

	self.password = function() {
		return password;
	}

	return self;
}