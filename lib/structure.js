module.exports = function(cerus) {
	var self = {};
	var structure = {};
	var event;

	cerus.promise(function(event_) {
		event = event_;	
	});

	self.add = function(name, slug, type, settings) {
		structure[slug] = {
			"name": name,
			"slug": slug,
			"type": type,
			"settings": settings || {}
		}

		event("add", structure[slug]);
	}

	self.get = function(slug) {
		return structure[slug];
	}

	self.remove = function(slug) {
		event("remove", structure[slug]);

		delete structure[slug];
	}

	self.list = function() {
		return structure;
	}

	self.event = function() {
		return event;
	}

	self.types = function() {
		var self_ = {};

		self_.image = function() {
			return "image";
		}

		self_.images = function() {
			return "images";
		}

		self_.checkbox = self_.boolean = function() {
			return "checkbox";
		}

		self_.text = function() {
			return "text";
		}

		self_.dropdown = self_.select = self_.options = function() {
			return "dropdown";
		}

		self_.range = function() {
			return "range";
		}

		self_.number = function() {
			return "number";
		}

		self_.textarea = function() {
			return "textarea";
		}

		self_.noinput = function() {
			return "noinput";
		}

		return self_;
	}

	return self;
}