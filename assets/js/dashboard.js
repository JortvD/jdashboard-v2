function nav() {
	var body = document.getElementsByClassName("body")[0];

	if(body.className.endsWith("small")) {
		body.className = "body full";
	}
	else {
		body.className = "body small";
	}
}

var current_section;

var cerus = cerus();
var api = cerus.api();

function select_section(section) {
	console.log("Selected SECTION (" + section + ")");

	var button = document.getElementById("nav-" + section);
	var buttons = document.getElementsByClassName("nav-link");

	for(var i = 0; i < buttons.length; i++) {
		buttons[i].className = "nav-link";
	}

	button.className = "nav-link selected";

	api.request("/" + section + "/get").emit()
	.then(function(val) {
		document.getElementById("body-title").innerHTML = val["section"]["name"];

		current_section = val["section"]

		var list = document.getElementsByClassName("list-links")[0];
		list.innerHTML = "";

		var pages = val["pages"];
		var keys = Object.keys(pages);

		if(keys.length == 0) {
			document.getElementById("tab-title").innerHTML = "";
			document.getElementsByClassName("tab-items")[0].innerHTML = "";
			document.getElementsByClassName("tab-actions")[0].innerHTML = "";
		}

		for(var i = 0; i < keys.length; i++) {
			var page = pages[keys[i]];
			var el = document.createElement("div");

			el.className = "list-link";
			el.id = "list-" + page.id;
			el.innerHTML = page.name === null ? "Onbekend" : page.name;
			el.addEventListener("click", function(id) {
				select_page(section, id);
			}.bind(null, page.id));

			list.appendChild(el);

			if(i == 0) {
				select_page(section, page.id);
			}
		}

		if(val["section"]["creatable"]) {
			var el = document.createElement("div");

			el.className = "list-link create";
			el.addEventListener("click", function(section_) {
				create_page(section_);
			}.bind(null, section));

			list.appendChild(el);
		}
	});
}

function save_page(section, page) {
	console.log("Saved PAGE (" + page.id + ")");

	var items_ = {};
	var items = page.items;

	for(var key in items) {
		var item = items[key];
		var value = null;

		if(item.type == "checkbox") {
			value = document.getElementById("checkbox-" + key).checked;
		}
		else if(item.type == "text") {
			value = document.getElementById("text-" + key).value;
		}
		else if(item.type == "textarea") {
			value = document.getElementById("textarea-" + key).value;
		}
		else if(item.type == "range") {
			value = document.getElementById("range-" + key).value;
		}
		else if(item.type == "number") {
			value = document.getElementById("number-" + key).value;
		}
		else if(item.type == "dropdown") {
			value = document.getElementById("dropdown-" + key).value;
		}
		else if(item.type == "image") {
			value = images_[key];
		}

		if(value === undefined || value === "") {
			value = null;
		}

		items_[key] = {
			"key": key,
			"value": value
		}

		if(item.type == "image") {
			items_[key].original = item.value;
		}
	}

	var request = api.request("/" + section + "/save");
	request.param("items", items_);
	request.param("page", page.id);
	request.emit()
	.then(function(val) {
		select_page(section, page.id);
	});
}

function delete_page(section, page) {
	console.log("Deleted PAGE (" + page.id + ")");

	var request = api.request("/" + section + "/delete");
	request.param("page", page.id);
	request.emit()
	.then(function(val) {
		select_section(section);
	});
}

function select_page(section, id) {
	console.log("Selected PAGE (" + id + ")");

	var button = document.getElementById("list-" + id);
	var buttons = document.getElementsByClassName("list-links")[0];

	for(var i = 0; i < buttons.childNodes.length; i++) {
		if(!buttons.childNodes[i].className.endsWith("create")) {
			buttons.childNodes[i].className = "list-link";
		}
	}

	button.className = "list-link selected";

	var request = api.request("/" + section + "/get");
	request.param("page", id);
	request.emit()
	.then(function(val) {
		var name = val["page"]["name"] === null ? "Onbekend" : val["page"]["name"];
		
		document.getElementById("tab-title").innerHTML = name;
		document.getElementsByClassName("list-link selected")[0].innerHTML = name; 

		var items = document.getElementsByClassName("tab-items")[0];
		items.innerHTML = "";
		var items_ = val["page"]["items"];

		for(var key in items_) {
			var item = items_[key];
			build_item(items, item);
		}

		var actions = document.getElementsByClassName("tab-actions")[0];
		actions.innerHTML = "";

		if(current_section.saveable) {
			var button = document.createElement("button");
			button.className = "button primary";
			button.innerHTML = "Sla op";
			button.addEventListener("click", function() {
				save_page(section, val["page"]);
			});
			actions.appendChild(button);
		}

		if(current_section.deleteable) {
			var button = document.createElement("button");
			button.className = "button secondary";
			button.innerHTML = "Verwijder";
			button.addEventListener("click", function() {
				if(window.confirm("Bent u zeker dat u het wilt verwijderen?")) {
					delete_page(section, val["page"]);
				}
			});
			actions.appendChild(button);
		}
	});
}

function build_item(parent, item) {

	var item_ = document.createElement("div");
	item_.className = "tab-item";

	var key = document.createElement("div");
	key.className = "item-key";
	key.innerHTML = item.name;
	item_.appendChild(key);

	var value = document.createElement("div");
	value.className = "item-value";
	
	if(item.type == "checkbox") {
		var checkbox = document.createElement("div");
		checkbox.className = "checkbox";

		var input = document.createElement("input");
		input.type = "checkbox";
		input.id = "checkbox-" + item.slug;
		input.checked = item.value;
		checkbox.appendChild(input);

		var label = document.createElement("label");
		label.htmlFor = "checkbox-" + item.slug;
		checkbox.appendChild(label);

		checkbox.appendChild(document.createElement("div"))
		value.appendChild(checkbox);
	}
	else if(item.type == "text") {
		var text = document.createElement("div");
		text.className = "text";

		var input = document.createElement("input");
		input.type = "text";
		input.id = "text-" + item.slug;

		if(item.settings && item.settings.placeholder) {
			input.placeholder = item.settings.placeholder;
		}
		else {
			input.placeholder = item.name + "...";
		}

		if(item.value) {
			input.value = item.value;
		}

		text.appendChild(input);
		value.appendChild(text);
	}
	else if(item.type == "textarea") {
		var textarea = document.createElement("div");
		textarea.className = "textarea";

		var input = document.createElement("textarea");
		input.id = "textarea-" + item.slug;

		if(item.value) {
			input.value = item.value;
		}

		if(item.settings && item.settings.placeholder) {
			input.placeholder = item.settings.placeholder;
		}
		else {
			input.placeholder = item.name + "...";
		}

		textarea.appendChild(input);
		value.appendChild(textarea);
	}
	else if(item.type == "range") {
		var range = document.createElement("div");
		range.className = "range";

		var input = document.createElement("input");
		input.type = "range";
		input.id = "range-" + item.slug;

		if(item.value) {
			input.value = item.value;
		}

		if(item.settings) {
			if(item.settings.placeholder) {
				input.placeholder = item.settings.placeholder;
			}

			if(item.settings.step) {
				input.step = item.settings.step;
			}

			if(item.settings.min) {
				input.min = item.settings.min;
			}

			if(item.settings.max) {
				input.max = item.settings.max;
			}
		}

		range.appendChild(input);
		value.appendChild(range);
	}
	else if(item.type == "number") {
		var number = document.createElement("div");
		number.className = "number";

		var input = document.createElement("input");
		input.type = "number";
		input.id = "number-" + item.slug;

		if(item.value) {
			input.value = item.value;
		}

		if(item.settings && item.settings.placeholder) {
			input.placeholder = item.settings.placeholder;
		}
		else {
			input.placeholder = "0";
		}

		if(item.settings) {
			if(item.settings.step) {
				input.step = item.settings.step;
			}

			if(item.settings.min) {
				input.min = item.settings.min;
			}

			if(item.settings.max) {
				input.max = item.settings.max;
			}
		}

		number.appendChild(input);
		value.appendChild(number);
	}
	else if(item.type == "noinput") {
		var noinput = document.createElement("div");
		noinput.className = "noinput";
		noinput.innerHTML = item.value;
		value.appendChild(noinput);
	}
	else if(item.type == "dropdown") {
		var dropdown = document.createElement("div");
		dropdown.className = "dropdown";

		var input = document.createElement("select");
		input.id = "dropdown-" + item.slug;

		if(item.settings && item.settings.options) {
			var options = item.settings.options;

			for(var i = 0 ; i < options.length; i++) {
				var option = options[i];
				var option_ = document.createElement("option");
				option_.value = option.slug;
				option_.innerHTML = option.name;
				input.appendChild(option_);
			}
		}

		if(value == null) {
			input.value = undefined;
		}
		else {
			input.value = item.value;
		}
		dropdown.appendChild(input);
		value.appendChild(dropdown);
	}
	else if(item.type == "image") {
		var images = document.createElement("div");
		images.className = "images";

		var image = document.createElement("div");
		image.appendChild(document.createElement("span"));
		
		if(item.value !== null) {
			images_[item.slug] = item.value;
			image.style.background = "url(" + item.value + ")";
			image.style.backgroundSize = "cover";
			image.style.backgroundRepeat = "no-repeat";
			image.style.backgroundPosition = "center center";
		}

		var file = document.createElement("input");
		file.type = "file";
		file.id = "image-" + item.slug;
		file.accept = "image/*";
		file.style.display = "none";
		file.addEventListener("change", function(e) {
			file_ = e.target.files[0];

			if(file_ === undefined) {
				return;
			}

			var reader = new FileReader();
			reader.readAsDataURL(file_);

			reader.onload = function (evt) {
				images_[item.slug] = evt.target.result;
				image.style.background = "url(" + images_[item.slug] + ")";
				image.style.backgroundSize = "cover";
				image.style.backgroundRepeat = "no-repeat";
				image.style.backgroundPosition = "center center";

				var imageClone = image.cloneNode(true);
				while(images.hasChildNodes()) {
    				images.removeChild(images.lastChild);
				}
				image = imageClone;
				images.appendChild(image);

				image_remove(item, file, image, images);
			}
		});
		image.appendChild(file);

		if(item.value === null) {
			image_new(item, file, image, images);
		}
		else {
			image_remove(item, file, image, images);
		}

		images.appendChild(image);
		value.appendChild(images);
	}

	item_.appendChild(value);
	parent.appendChild(item_)
}

var images_ = {};


function image_new(item, file, image, images) {
	image.className = "image new";

	image.addEventListener("click", function() {
		file.click();
	});
}

function image_remove(item, file, image, images) {
	image.className = "image remove";
	file.value = "";

	image.addEventListener("click", function() {
		images_[item.slug] = null;
		image.style.background = "";

		var imageClone = image.cloneNode(true);
		while(images.hasChildNodes()) {
			images.removeChild(images.lastChild);
		}
		image = imageClone;
		images.appendChild(image);

		image_new(item, file, image, images);
	});
}

function create_page(section) {

	api.request("/" + section + "/create").emit()
	.then(function(val) {
		var list = document.getElementsByClassName("list-links")[0];
		var el = document.createElement("div");

		el.className = "list-link";
		el.innerHTML = val.name;
		el.id = "list-" + val.id;
		el.addEventListener("click", function(id) {
			select_page(section, id);
		}.bind(null, val.id));

		console.log("Created PAGE (" + val.id + ")");

		list.insertBefore(el, list.childNodes[list.childNodes.length - 1]);
		select_page(section, val.id);
	});
}

api.request("/info").emit()
.then(function(val) {
	document.getElementById("info-name").innerHTML = val["user"]["username"];
	document.getElementById("info-position").innerHTML = val["user"]["role"];

	var sections = val["sections"];
	var keys = Object.keys(sections);
	var links = document.getElementsByClassName("nav-links")[0];

	for(var i = 0; i < keys.length; i++) {
		var el = document.createElement("div");
		var section = sections[keys[i]];

		el.className = "nav-link";
		el.id = "nav-" + section.slug;
		el.innerHTML = section.name;
		el.addEventListener("click", function(slug, name) {
			select_section(slug, name);
		}.bind(null, section.slug, section.name));
		links.appendChild(el);

		if(i == 0) {
			select_section(section.slug);
		}
	}
});

