var cerus = function() {
	var self = {};

	self.api = function() {
		return api_;
	}

	self.settings = function() {
		return settings_;
	}

	self.promise = function(func) {
		return new promise(func);
	}

	self.request = function(url, method) {
		return new request(self, url, method);
	}

	var settings_ = new settings(self);
	var api_ = new api(self);

	return self;
}

var api = function(cerus) {
	var self = {};

	self.request = function(url, method) {
		url = url || "";
		method = method || "POST";

		url = cerus.settings().root() + cerus.settings().urls().api() + url;

		return cerus.request(url, method);
	}

	return self;
}

var request = function(cerus, url, method) {
	var self = {};

	url = url || "";
	method = method || "GET";

	var headers = {};
	var params = {};
	var mime = null

	self.url = function(url_) {
		if(url_ != null) {
			url = url_;
		}

		return url;
	}

	self.method = function(method_) {
		if(method_ != null) {
			method = method_;
		}

		return method;
	}

	self.mime = function(mime_) {
		if(mime_ != null) {
			mime = mime_;
		}

		return mime;
	}

	self.header = function(key, value) {
		headers[key] = value;
	}

	self.param = function(key, value) {
		params[key] = value;
	}

	self.emit = function() {
		return cerus.promise(function(event) {
			var xhr = new XMLHttpRequest();
			var data = null;

			if(method == "POST") {
				data = {};

				for(var key in params) {
					if(params.hasOwnProperty(key)) {
						data[key] = params[key];
					}
				}
			}

			if(mime != null) {
				xhr.overrideMimeType(mime);
			}

			xhr.onreadystatechange = function() {
			    if(xhr.readyState == XMLHttpRequest.DONE) {
			    	var val;
			    	try {
						val = JSON.parse(xhr.responseText);
					} catch (e) {
						val = xhr.responseText;
					}

					event("response", val);
				}
			}

			xhr.open(method, url);

			for(var key in headers) {
				if(headers.hasOwnProperty(key)) {
					xhr.setRequestHeader(key, headers[key]);
				}
			}

			if(data != null) {
				xhr.send(JSON.stringify(data));
			}
			else {
				xhr.send();
			}
		});
	}

	return self;
}

var promise = function(func) {
	var self = {};

	var funcs = {};
	var locked = true;
	var stacked = null;

	setTimeout(function() {
		locked = false;
		self.update();
	}, 0);

	/**
	 * @function update
	 */
	self.update = function() {
		if(locked) {
			return;
		}

		for(var i = 0; i < self.queue.length; i++) {
			var event_ = self.queue[i];

			execute(funcs[event_.event], event_.args);

			if(event_.event == "err" || event_.event == "catch" || event_.event == "failure") {
				execute(funcs["error"], event_.args);
			}
			else if(event_.event != "error") {
				execute_then(event_.event, funcs["then"], event_.args);
				execute(funcs["then2"], event_.args);
			}

			self.queue.splice(i);
		}
	}

	var execute = function(funcs_, args) {
		if(funcs_ == null) {
			return;
		}

		for(var i = 0; i < funcs_.length; i++) {
			funcs_[i](...args);
		}
	}

	var execute_then = function(event_, funcs_, args) {
		if(funcs_ == null) {
			return;
		}

		for(var i = 0; i < funcs_.length; i++) {
			funcs_[i](event_, ...args);
		}
	}

	self.queue = [];

	self.event = function(event_, ...args_) {
		if(event_ == null) {
			return self;
		}

		if(stacked != null) {
			stacked(event_, args_);
			return self;
		}

		self.queue[self.queue.length] = {
			args: args_,
			event: event_
		}

		self.update();

		return self;
	}

	self.then = function(func_, event_) {
		if(func_ == self.event) {
			return self.stack(func_);
		}

		event_ = event_ ? "then" : "then2";

		if(funcs[event_] == null) {
			funcs[event_] = [];
		}

		funcs[event_][funcs[event_].length] = func_;

		self.update();
	
		return self;
	}

	self.catch = function(func_) {
		if(func_ == self.event) {
			return self.stack(func_);
		}

		if(funcs["error"] == null) {
			funcs["error"] = [];
		}

		funcs["error"][funcs["error"].length] = func_;

		self.update();
	
		return self;
	}

	self.on = function(event_, func_) {
		if(func_ == self.event) {
			return self.stack(func_);
		}

		if(funcs[event_] == null) {
			funcs[event_] = [];
		}

		funcs[event_][funcs[event_].length] = func_;

		self.update();
	
		return self;
	}

	self.stack = function(event_) {
		locked = false;

		var promise_ = event_();

		stacked = promise_.event;
		promise_.queue = promise_.queue.concat(self.queue);
		promise_.update();
	
		return self;
	}

	func.apply(self, [self.event]);
	
	return self;
}

var settings = function(cerus) {
	var self = {};
	var settings = {};

	self.setting = function(key, default_, func) {
		settings[key] = default_;

		if(func != null) {
			func(default_);
		}

		var keys = key.split(".");
		var self_ = self;

		for(var i = 0; i < keys.length; i++) {
			var key_ = keys[i];

			if(i < (keys.length - 1)) {
				if(self_[key_] == null) {
					var self__ = {};

					if(self_ instanceof Function) {
						self_()[key_] = function() {
							return self__;
						}
					}
					else {
						self_[key_] = function() {
							return self__;
						}
					}
					
					self_ = self__;
				}
				else {
					self_ = self_[key_]();
				}
			}
			else {
				var func_ = function(value) {
					if(value != null) {
						settings[key] = value;

						if(func != null) {
							func(default_);
						}
					}

					return settings[key];
				}

				if(self_ instanceof Function) {
					self_()[key_] = func_;
				}
				else {
					self_[key_] = func_;
				}
			}
		}
	}

	self.setting("root", window.location.protocol + "//" + window.location.hostname + (window.location.port ? ":" + window.location.port : "") + "/");
	self.setting("urls.api", "api");

	return self;
}
