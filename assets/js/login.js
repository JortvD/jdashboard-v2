var cerus = cerus();
var api = cerus.api();

document.getElementById("login")
.addEventListener("click", function() {
	var login = api.request("/login");
	login.param("username", document.getElementById("username").value);
	login.param("password", document.getElementById("password").value);
	login.emit()
	.then(function(val) {
		if(val["code"] == 200) {
			window.location.href = cerus.settings().root() + "dashboard";
		}
		else {
			var login = document.getElementById("login")
			var original_color = login.style.background;
			var original_transition = login.style.transition;
			login.style.background = "#e9513a";
			login.style.borderColor = "#e9513a";
			login.style.transition = "0.8s all";
			setTimeout(function() {
				login.style.background = original_color;
				login.style.borderColor = original_color;
			}, 1000);
			setTimeout(function() {
				login.style.transition = original_transition;
			}, 2000);
		}
	});
});